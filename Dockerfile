FROM golang:1.9 AS build_env

WORKDIR /go/src/app
ADD . .

RUN go get -u github.com/golang/dep/cmd/dep
RUN dep ensure
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM scratch
ADD https://raw.githubusercontent.com/nownabe/ca-certificates/master/ca-certificates.crt /etc/ssl/certs/
COPY --from=build_env /go/src/app/app /

CMD ["/app"]
