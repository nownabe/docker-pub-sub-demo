Demo: Pub/Sub with Golang on Docker
===================================

# Prerequisites
* Golang
* Ruby (Sending message)
* GCP Project
* GCP Service Account (Pub/Sub Editor)

# Configurations

* `PROJECT_ID`: GCP project ID
* `TOPIC_ID`: Pub/Sub topic id
* `SUBSCRIPTION_ID`: Pub/Sub subscription id
* (optional) `GOOGLE_APPLICATION_CREDENTIALS`: Path to GCP service account key JSON file

# Run

```bash
docker run \
  -e PROJECT_ID=${PROJECT_ID} \
  -e TOPIC_ID=${TOPIC_ID} \
  -e SUBSCRIPTION_ID=${SUBSCRIPTION_ID} \
  -e GOOGLE_APPLICATION_CREDENTIALS=/root/credentials.json \
  -v `pwd`/credentials.json:/root/credentials.json \
  registry.gitlab.com/nownabe/docker-pub-sub-demo
```

Send message:

```bash
bundle install

# Set PROJECT_ID and TOPIC_ID
bundle exec ruby send.rb
```

# on GCE (Container-Optimized OS)

Create an instance:

```bash
gcloud compute instances create ${INSTANCE_NAME} \
  --machine-type f1-micro \
  --zone asia-northeast1-a \
  --image-project cos-cloud \
  --image-family cos-stable
```

First deploy:

```bash
gcloud compute ssh ${INSTANCE_NAME} -- \
  docker run \
    --name demo \
    -d \
    -e PROJECT_ID=${PROJECT_ID} \
    -e TOPIC_ID=${TOPIC_ID} \
    -e SUBSCRIPTION_ID=${SUBSCRIPTION_ID} \
    registry.gitlab.com/nownabe/docker-pub-sub-demo
```

Note: GCE default service account includes Pub/Sub scope.

Send messages and see logs:

```bash
$ bundle exec ruby send.rb
$ gcloud compute ssh ${INSTANCE_NAME} -- docker logs demo
2018/02/16 13:00:43 Received: &{Message:Hello, Pub/Sub}
```

Deploy:

```bash
# Stop the container
gcloud compute ssh ${INSTANCE_NAME} -- docker kill demo

# Remove the container
gcloud compute ssh ${INSTANCE_NAME} -- docker rm demo

# Pull a new image
gcloud compute ssh ${INSTANCE_NAME} -- \
  docker pull registry.gitlab.com/nownabe/docker-pub-sub-demo

# Run container from a new image
gcloud compute ssh ${INSTANCE_NAME} -- \
  docker run \
    --name demo \
    -d \
    -e PROJECT_ID=${PROJECT_ID} \
    -e TOPIC_ID=${TOPIC_ID} \
    -e SUBSCRIPTION_ID=${SUBSCRIPTION_ID} \
    registry.gitlab.com/nownabe/docker-pub-sub-demo
```
