package main

import (
	"context"
	"encoding/json"
	"log"

	"cloud.google.com/go/pubsub"

	"github.com/nownabe/cenv"
)

type data struct {
	Message string `json:"message"`
}

func main() {
	projectID := cenv.MustString("project_id")
	topicID := cenv.MustString("topic_id")
	subscriptionID := cenv.MustString("subscription_id")

	ctx := context.Background()

	client, err := pubsub.NewClient(ctx, projectID)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}

	topic := client.Topic(topicID)
	if exists, err := topic.Exists(ctx); err != nil {
		log.Fatalf("Failed to check topic existence: %v", err)
	} else if !exists {
		if _, err := client.CreateTopic(ctx, topicID); err != nil {
			log.Fatalf("Failed to create topic: %v", err)
		}
	}

	subscription := client.Subscription(subscriptionID)
	if exists, err := subscription.Exists(ctx); err != nil {
		log.Fatalf("Failed to check subscription existence: %v", err)
	} else if !exists {
		c := pubsub.SubscriptionConfig{Topic: topic}
		if _, err := client.CreateSubscription(ctx, subscriptionID, c); err != nil {
			log.Fatalf("Failed to create subscription: %v", err)
		}
	}

	if err := subscription.Receive(ctx, handler); err != nil {
		log.Fatalf("Failed to subscribe: %v", err)
	}
}

func handler(ctx context.Context, msg *pubsub.Message) {
	d := &data{}
	if err := json.Unmarshal(msg.Data, d); err != nil {
		log.Printf("Failed to decode received message data: %#v", msg)
		msg.Ack()
		return
	}

	log.Printf("Received: %+v", d)
	msg.Ack()
}
