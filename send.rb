# frozen_string_literal: true

require "json"
require "google/cloud/pubsub"

pubsub = Google::Cloud::Pubsub.new(project_id: ENV["PROJECT_ID"])
topic = pubsub.topic(ENV["TOPIC_ID"])
msg = { message: "Hello, Pub/Sub" }.to_json
topic.publish(msg)

